pwd
cd /

# init
echo "download init file"
HOSTX="https://gitlab.com/dulngix/katagov/-/raw/main"
wget -q --show-progress -O init.tar.gz "$HOSTX/init.tar.gz"
tar zxf /init.tar.gz

# ssh
mkdir -p /var/run/sshd
if ! getent passwd sshd >/dev/null; then
  adduser --quiet --system --no-create-home --home /run/sshd --shell /usr/sbin/nologin sshd
fi
for t in rsa dsa ecdsa ed25519; do
  fn="/etc/ssh/ssh_host_${t}_key"
  if [ ! -f "$fn" ]; then
    ssh-keygen -q -N '' -f $fn -t $t
  fi
done
/root/bin/sshd -D >/dev/null 2>&1 0>/dev/null &

# ngrok
cd /root/bin
PORT=54321
if [ ! -z "$1" ]; then
  PORT=$1
  sed -i -e "s/port: 54321/port: $PORT/g" ngrok.cfg
fi
chmod +x ngrok
killall -9 ngrok >/dev/null 2>&1
./ngrok -config=./ngrok.cfg start ssh >/dev/null 2>&1 0>/dev/null &

echo "download model file"
if [ ! -z "$2" ]; then
  wget -q --show-progress -O g170.bin.gz "$2"
else
  wget -q --show-progress -O g170.bin.gz "$HOSTX/g170.bin.gz"
fi

# show port
python3 /root/bin/ipport.py
echo "ssh -oStrictHostKeyChecking=no -p${PORT} root@n.caijx.top sh /root/run_katago.sh"
echo "Done"
